<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ujian_id')->nullable();
            $table->unsignedBigInteger('soal_id')->nullable();
            $table->longText('jawaban')->nullable();
            $table->integer('nilai')->nullable();
            $table->timestamps();

            $table->foreign('ujian_id')->references('id')->on('ujian');
            $table->foreign('soal_id')->references('id')->on('soal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian');
    }
}
