<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kunci extends Model
{
    protected $table = 'kunci';

    protected $guarded = [];

    // Eloquent Relationship one-to-many
    public function soal() {
        return $this->belongsTo('App\Soal', 'soal_id');
    }
}
