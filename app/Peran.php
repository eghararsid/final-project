<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = "peran";

    protected $guarded = [];

    // Eloquent Relationship one-to-one
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
