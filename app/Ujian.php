<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    protected $table = 'ujian';

    protected $guarded = [];

    // Eloquent Relationship many-to-many
    public function penilaian() {
        return $this->belongsToMany('App\Soal', 'penilaian', 'ujian_id', 'soal_id');
    }
}
