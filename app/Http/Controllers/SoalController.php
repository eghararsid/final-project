<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Soal;
use App\Paket;
use App\Kunci;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SoalController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soals = Soal::latest()->paginate(10);

        return view('soal.index', [
            'soals' => $soals
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Paket $paket)
    {
        return view('guru.soal.create', compact('paket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Paket $paket, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal' => 'required',
            'jawaban' => 'required',
            'nilai' => 'required',
            'keyword1' => 'required',
            'keyword2' => 'required',
            'keyword3' => 'required',
            'keyword4' => 'required',
            'keyword5' => 'required'
        ]);

        if ($validator->fails()) {
            Alert::error('Oops!', 'There are blank input fields');
            return redirect()->route('guru.soal.create', ['paket' => $paket->id]);
        }

        if ($request->nilai <= 0) {
            Alert::error('Oops!', 'The score field must be greater that zero (0)');
            return redirect()->route('guru.soal.create', ['paket' => $paket->id]);
        }

        $new = Soal::create([
            'soal' => $request['soal'],
            'jawaban' => $request['jawaban'],
            'nilai' => $request['nilai'],
            'paket_id' => $paket->id
        ]);

        for ($i = 1; $i < 6; $i++) { 
            Kunci::create([
                'soal_id' => $new->id,
                'kunci_1' => $request['keyword'.$i],
                'kunci_2' => $request['keyword'.$i.'_alt1'],
                'kunci_3' => $request['keyword'.$i.'_alt2'],
                'nilai' => $request['nilai']/5
            ]);
        }

        $soal = Soal::where('paket_id', $paket->id)->get();

        Alert::success('Congratulations!', 'You Have Added New Questions');
        return redirect()->route('guru.soal.create', ['paket' => $paket->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket, Soal $soal)
    {
        return view('guru.soal.show', compact('paket', 'soal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket, Soal $soal)
    {
        $request->validate([
            'soal' => 'required',
            'jawaban' => 'required',
            'nilai' => 'required|numeric',
        ]);

        $soal->update($request->all());
        $soal = Soal::where('paket_id', $paket->id)->get();

        Alert::success('Congratulations!', 'Your Question Successfuly Updated');
        return view('guru.soal.show', compact('paket', 'soal'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paket $paket, Soal $soal)
    {
        $soal->kunci()->delete();
        $soal->penilaian()->delete();
        $soal->delete();

        Alert::success('Congratulations!', 'Your Question Successfuly Deleted');
        return redirect()->route('guru.soal.create', compact('paket'));
    }
}
