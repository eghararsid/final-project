<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Paket;
use App\Soal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class PaketController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function validasiWaktu($mulai, $selesai, $request)
    {
        $now = Carbon::now();

        if ($mulai > $selesai || $request->durasi <= 0) {
            Alert::error('Oops!', 'There Are Errors On Your Time Input');
            return true;
        }
        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pakets = Paket::where('user_id', Auth::user()->id)->get();
        
        $count = [];
        foreach ($pakets as $key => $value) {
            $soal = Soal::where('paket_id', $value->id)->get();
            $count[] = count($soal);
        }

        return view('guru.paket.index', [
            'pakets' => $pakets,
            'soal' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guru.paket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pelajaran' => 'required',
            'password' => 'required',
            'durasi' => 'required',
            'openedDate' => 'required',
            'openedTime' => 'required',
            'closedDate' => 'required',
            'closedTime' => 'required'
        ]);

        if ($validator->fails()) {
            Alert::error('Oops!', 'There Are Blank Input Fields');
            return redirect()->route('guru.paket.create');
        }

        $mulai = date("Y-m-d G:i:s", strtotime($request->openedDate." ".$request->openedTime));
        $selesai = date("Y-m-d G:i:s", strtotime($request->closedDate." ".$request->closedTime));
        if ($this->validasiWaktu($mulai, $selesai, $request)) {
            return redirect()->route('guru.paket.create');
        }

        $id = Auth::id();
        $create = Paket::create([
            'pelajaran' => $request['pelajaran'],
            'password' => $request['password'],
            'durasi' => $request['durasi'],
            'user_id' => $id,
            'mulai' => $mulai,
            'selesai' => $selesai
        ]);

        $create->update([
            'paket' => strtolower($request['pelajaran'].$create->id),
        ]);

        Alert::success('Congratulations!', 'Your Package Successfully Created');
        return redirect()->route('guru.paket.show', ['paket' => $create->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket)
    {
        $soal = Soal::where('paket_id', $paket->id)->get();
        $openedDate = date('d F Y', strtotime($paket->mulai));
        $closedDate = date('d F Y', strtotime($paket->selesai));
        $openedTime = date('h:i a', strtotime($paket->mulai));
        $closedTime = date('h:i a', strtotime($paket->selesai));

        return view('guru.paket.show', compact('paket', 'soal', 'openedDate', 'openedTime', 'closedDate', 'closedTime'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'durasi' => 'required',
            'openedDate' => 'required',
            'closedDate' => 'required',
            'openedTime' => 'required',
            'closedTime' => 'required'
        ]);

        if ($validator->fails()) {
            Alert::error('Oops!', 'There Are Blank Input Fields');
            return redirect()->route('guru.paket.show', ['paket' => $paket->id]);
        }

        $mulai = date("Y-m-d G:i:s", strtotime($request->openedDate." ".$request->openedTime));
        $selesai = date("Y-m-d G:i:s", strtotime($request->closedDate." ".$request->closedTime));
        if ($this->validasiWaktu($mulai, $selesai, $request)) {
            return redirect()->route('guru.paket.show', ['paket' => $paket->id]);
        }

        $paket->update([
            'password' => $request['password'],
            'durasi' => $request['durasi'],
            'mulai' => $mulai,
            'selesai' => $selesai
        ]);

        Alert::success('Congratulations!', 'Your Package Successfully Updated');
        return redirect()->route('guru.paket.show', ['paket' => $paket->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paket $paket)
    {
        foreach ($paket->soal as $key => $value) {
            $value->kunci()->delete();
        }

        foreach ($paket->soal as $key => $value) {
            $value->penilaian()->delete();
        }

        $paket->ujian()->delete();
        $paket->soal()->delete();
        $paket->delete();

        Alert::success('Congratulations!', 'Your Package Successfully Deleted');
        return redirect()->route('guru.paket.index')->with('success','Paket deleted successfully');
    }
}
