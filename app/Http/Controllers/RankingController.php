<?php

namespace App\Http\Controllers;

use App\Ujian;
use App\User;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function show($nilai)
    {
        $rank = Ujian::where('paket_id', $nilai)->orderBy('nilai', 'desc')->get();
        $name = [];
        foreach ($rank as $key => $value) {
            $temp = User::where('id', $value->user_id)->first();
            $name[] = $temp->name;
        }

        return view('murid.nilai.rank', [
            'rank' => $rank,
            'name' => $name   
        ]);
    }
}
