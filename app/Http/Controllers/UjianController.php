<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Ujian;
use App\Paket;
use App\User;
use App\Penilaian;
use App\Soal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class UjianController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function validasiWaktu($mulai, $selesai)
    {
        $now = Carbon::now();

        if ($now < $mulai) {
            $mulai = date('d F Y G:i:s', strtotime($mulai));
            Alert::error('Oops!', 'The Exam will be started at '.$mulai);
            return true;
        } elseif ($now > $selesai) {
            $mulai = date('d F Y G:i:s', strtotime($selesai));
            Alert::error('Oops!', 'The Exam was closed at '.$selesai);
            return true;
        }
        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujian = Ujian::where('user_id', Auth::user()->id)->get();
        $countSoal = [];
        $countMurid = [];
        $paket = [];
        foreach ($ujian as $key => $value) {
            $soal = Soal::where('paket_id', $value->paket_id)->get(); 
            $countSoal[] = count($soal);
            $murid = Ujian::where('paket_id', $value->paket_id)->get();
            $countMurid[] = count($murid);
            $cek = Paket::where('id', $value->paket_id)->first();
            $paket[] = $cek;
        }

        return view('murid.ujian.index', [
            'ujian' => $ujian,
            'soal' => $countSoal,
            'murid' => $countMurid,
            'paket' => $paket
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('murid.ujian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'paket' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            Alert::error('Oops!', 'There Are Blank Input Fields');
            return redirect()->route('murid.ujian.create');
        }

        $id = strtolower($request->paket);
        $pass = $request->password;
        
        $paket = Paket::where([
            ['paket', '=', $id],
            ['password', '=', $pass]
        ])->first();
        
        $ujian = 0;
        if ($paket !== null) {
            $check = Ujian::where([
                ['paket_id', '=', $paket->id],
                ['user_id', '=', Auth::user()->id]
            ])->first();

            if ($check === null) {

                $ujian = Ujian::create([
                    'paket_id' => $paket->id,
                    'user_id' => Auth::user()->id,
                    'nilai' => 0,
                    'submit' => 0,
                    'start' => 0
                ]);

            }

            $ujian = Ujian::where([
                ['paket_id', '=', $paket->id],
                ['user_id', '=', Auth::user()->id]
            ])->first();

            if ($ujian->submit == 1) {
                Alert::error('Sorry!', 'You Have Joined This Exam');
                return redirect()->route('murid.ujian.index');
            }

            $mulai = $paket->mulai;
            $selesai = $paket->selesai;

            if ($this->validasiWaktu($mulai, $selesai)) {
                return redirect()->route('murid.ujian.index');
            }

            Alert::success('Exam Starts!', 'Stay Focus And Good Luck');
            return redirect()->route('murid.ujian.show', ['paket' => $paket, 'ujian' => $ujian]);
        } else {
            Alert::error('No Package Found!', 'We Cannot Find Your Requested Exam');
            return redirect()->route('murid.ujian.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket, Ujian $ujian)
    {
        if ($this->validasiWaktu($paket->mulai, $paket->selesai)) {
            return redirect()->route('murid.ujian.index');
        }

        if ($ujian->start == 0) {
            $ujian->update([
                'start' => 1,
                'finished_at' => Carbon::now()
            ]);
        }

        Alert::success('Exam Starts!', 'Stay Focus And Good Luck');
        return view('murid.ujian.show', compact('paket', 'ujian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket, Ujian $ujian)
    {
        $ujian->submit = 1;
        
        $soal = [];
        foreach ($paket->soal as $key => $value) {
            $soal[] = $value->id;
        }

        $ujian->penilaian()->sync($soal);

        $nilai = [];
        foreach ($soal as $key => $value) {
            $penilaian = Penilaian::where([
                ['ujian_id', '=', $ujian->id],
                ['soal_id', '=', $value]
            ])->first();
            $penilaian->jawaban = $request->jawaban[$key];
            $cocok = Soal::find($value);
            if ($penilaian->jawaban == $cocok->jawaban) {
                $penilaian->nilai = $cocok->nilai;
            } else {
                $penilaian->nilai = 0;
            }
            $nilai[] = $penilaian->nilai;
            $penilaian->save();
        }

        foreach ($nilai as $key => $value) {
            $ujian->nilai += $value;
        }
        $ujian->save();

        Alert::success('Congratulations!', 'You Cleared Examination Very Well');
        return redirect()->route('murid.ujian.index')->with('success', 'Ujian updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ujian $ujian)
    {
        $ujian->delete();

        return redirect()->route('ujian.index')->with('success', 'Ujian deleted successfully');
    }
}
