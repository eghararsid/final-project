<?php

namespace App\Http\Controllers;

use App\Penilaian;
use App\Ujian;
use App\Paket;
use App\Soal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nilai)
    {
        $soal = Soal::where('paket_id', $nilai)->get();
        $ujian = Ujian::where([
            ['paket_id', '=', $nilai],
            ['user_id', '=', Auth::user()->id]
        ])->first();

        $nilai = [];
        $jawaban = [];
        foreach ($soal as $key => $value) {
            $penilaian = Penilaian::where([
                ['ujian_id', '=', $ujian->id],
                ['soal_id', '=', $value->id]
            ])->first();
            $nilai[] = $penilaian->nilai;
            $jawaban[] = $penilaian->jawaban;
        }

        return view('murid.nilai.show', [
            'soal' => $soal,
            'nilai' => $nilai,
            'jawaban' => $jawaban
        ]);
    }
}
