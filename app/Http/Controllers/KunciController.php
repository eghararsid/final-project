<?php

namespace App\Http\Controllers;

use App\Kunci;
use Illuminate\Cache\RedisTaggedCache;
use Illuminate\Http\Request;

class KunciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kuncis = Kunci::latest()->paginate(10);

        return view('kunci.index', compact('kuncis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kunci.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kunci_1' => 'required',
            'kunci_2' => 'required',
            'kunci_3' => 'required',
            'nilai' => 'required|numeric'
        ]);


        Kunci::create($request->all());

        return redirect()->route('kunci.index')->with('success', 'Kunci successfully added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kunci $kunci)
    {
        return view('kunci.show', compact('kunci'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kunci $kunci)
    {
        return view('kunci.edit', compact('kunci'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kunci $kunci)
    {
        $request->validate([
            'kunci_1' => 'required',
            'kunci_2' => 'required',
            'kunci_3' => 'required',
            'nilai' => 'required|numeric'
        ]);

        $kunci->update($request->all());

        return redirect()->route('kunci.index')->with('success', 'Kunci successfully updated');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kunci $kunci)
    {
        $kunci->delete();

        return redirect()->route('kunci.index')->with('success', 'Kunci successfully deleted');
    }
}
