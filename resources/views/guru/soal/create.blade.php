@extends('layouts.guru')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css') }}">
@endpush

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mb-30">
        <form action="{{ route('guru.soal.store', ['paket' => $paket->id]) }}" method="POST">
            @csrf
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue h4">Create New Question</h4>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-12 col-md-2 col-form-label">Question</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="soal" type="soal" placeholder="Input Question Here!">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Answer</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="jawaban" placeholder="Input Answer Here!">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Score</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="nilai" placeholder="Input Score Here!" type="number">
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-6 col-md-2 col-form-label">Keyword 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword1" placeholder="Required">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword1_alt1" placeholder="Optional">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword1_alt2" placeholder="Optional">
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-6 col-md-2 col-form-label">Keyword 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword2" placeholder="Required">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword2_alt1" placeholder="Optional">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword2_alt2" placeholder="Optional">
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-6 col-md-2 col-form-label">Keyword 3</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword3" placeholder="Required">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword3_alt1" placeholder="Optional">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword3_alt2" placeholder="Optional">
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-6 col-md-2 col-form-label">Keyword 4</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword4" placeholder="Required">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword4_alt1" placeholder="Optional">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword4_alt2" placeholder="Optional">
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-6 col-md-2 col-form-label">Keyword 5</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword5" placeholder="Required">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 1</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword5_alt1" placeholder="Optional">
                </div>
                <label class="col-sm-6 col-md-2 col-form-label">Key Alternative 2</label>
                <div class="col-sm-6 col-md-2">
                    <input class="form-control" name="keyword5_alt2" placeholder="Optional">
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </form>
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Display All Questions</h4>
                <p>This page displays the information of all questions this package has, including Questions, Answers, Scores, Keywords. In here, you can also create new Question and view more details into your Question's information</p>
            </div>
        </div>
        <div class="pb-20">
            <table class="data-table table stripe hover nowrap">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Question</th>
                        <th scope="col">Answer</th>
                        <th scope="col">Score</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($paket->soal as $key => $item)
                        <tr>
                            <td class="table-plus" scope="row">{{ $key+1 }}</td>
                            <td>{{ $item->soal }}</td>
                            <td>{{ $item->jawaban }}</td>
                            <td>{{ $item->nilai }}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="dw dw-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                        <a class="dropdown-item" href="/guru/cek-paket/{{ $paket->id }}/{{ $item->id }}"><i class="dw dw-eye"></i> View Details</a>
                                        <form id="p" action="/guru/cek-paket/{{ $paket->id }}/{{ $item->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="p()" href="#" class="dropdown-item"><i class="dw dw-delete-3"></i> Delete Question</a>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>    
                    @empty
                        <tr>
                            <td colspan="6" align="center">No Questions Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script>
function p() {
    document.getElementById("p").submit();
  }
</script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/vendors/scripts/datatable-setting.js') }}"></script>
@endpush