@extends('layouts.guru')

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mt-3">
        <form action="{{ route('guru.paket.store') }}" method="POST">
            @csrf
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue h4">Create Package Forms</h4>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-12 col-md-2 col-form-label">Subject Name</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="pelajaran" value="{{ old('pelajaran') }}" placeholder="Fisika">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Package Password</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="password" value="{{ old('password') }}" placeholder="Enter Password">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Exam Duration (Minutes)</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" value="120" name="durasi" value="{{ old('durasi') }}" type="number">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Opened Date</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control date-picker" name="openedDate" value="{{ old('openedDate') }}" placeholder="Select Date" type="text">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Opened Time</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control time-picker" name="openedTime" value="{{ old('openedTime') }}" placeholder="Select time" type="text">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Closed Date</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control date-picker" name="closedDate" value="{{ old('closedDate') }}" placeholder="Select Date" type="text">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Closed Time</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control time-picker" name="closedTime" value="{{ old('closedTime') }}" placeholder="Select time" type="text">
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Create New Package</button>
                </div>
            </div>
        </form>
    </div>
@endsection