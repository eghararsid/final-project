@extends('layouts.guru')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css') }}">
@endpush

@section('content')
    <!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Display All Packages</h4>
                <p>This page displays the information of all packages you have, including Package Name, Password, Quiz Duration, Students Joined the Quiz. In here, you can also create new package and view more details into your package information</p>
            </div>
            <div class="pull-right">
                <a href="{{ route('guru.paket.create') }}" class="btn btn-primary btn-sm scroll-click">Create New</a>
            </div>
        </div>
        <div class="pb-20">
            <table class="data-table table stripe hover nowrap">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Package Name</th>
                        <th scope="col">Password</th>
                        <th scope="col">Questions</th>
                        <th scope="col">Opened At</th>
                        <th scope="col">Closed At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pakets as $key => $item)
                        <tr>
                            <td class="table-plus" scope="row">{{ $key+1 }}</td>
                            <td>{{ $item->paket }}</td>
                            <td>{{ $item->password }}</td>
                            <td>{{ $soal[$key] }}</td>
                            <td>{{ $item->mulai }}</td>
                            <td>{{ $item->selesai }}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="dw dw-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                        <a class="dropdown-item" href="/guru/cek-paket/{{ $item->id }}"><i class="dw dw-eye"></i> View Package</a>
                                        <a class="dropdown-item" href="/guru/cek-ujian/{{ $item->id }}"><i class="dw dw-eye"></i> View Exam</a>
                                        <form id="p" action="/guru/cek-paket/{{ $item->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="p()" href="#" class="dropdown-item"><i class="dw dw-delete-3"></i> Delete Package</a>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>    
                    @empty
                        <tr>
                            <td colspan="6" align="center">No Package Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <!-- Responsive tables End -->
@endsection

@push('scripts')
<script>
function p() {
    document.getElementById("p").submit();
  }
</script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/vendors/scripts/datatable-setting.js') }}"></script>
@endpush