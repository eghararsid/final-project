@extends('layouts.guru')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css') }}">
@endpush

@section('content')
<!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div>
                <h4 class="text-blue h4">Package Details</h4>
                <form action="{{ route('guru.paket.update', ['paket' => $paket->id]) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group row mt-4">
                        <label class="col-sm-12 col-md-2 col-form-label">Package Name</label>
                        <div class="col-sm-12 col-md-4">
                            <input class="form-control" name="paket" value="{{ old('paket', $paket->paket) }}" disabled placeholder="Unique">
                        </div>
                        <label class="col-sm-12 col-md-2 col-form-label">Subject</label>
                        <div class="col-sm-12 col-md-4">
                            <input class="form-control" name="pelajaran" value="{{ old('pelajaran', $paket->pelajaran) }}" disabled placeholder="Enter Subject">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Package password</label>
                        <div class="col-sm-12 col-md-4 pull-right">
                            <input class="form-control" name="password" value="{{ old('password', $paket->password) }}" placeholder="Enter Password">
                        </div>
                        <label class="col-sm-12 col-md-2 col-form-label">Duration (Minutes)</label>
                        <div class="col-sm-12 col-md-4 pull-right">
                            <input class="form-control" name="durasi" value="{{ old('durasi', $paket->durasi) }}" type="number" placeholder="Enter Duration">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Opened Date</label>
                        <div class="col-sm-12 col-md-4">
                            <input class="form-control date-picker" name="openedDate" placeholder="Opened Date & Time" value="{{ old('openedDate', $openedDate) }}">
                        </div>
                        <label class="col-sm-12 col-md-2 col-form-label">Closed Date</label>
                        <div class="col-sm-12 col-md-4 pull-right">
                            <input class="form-control date-picker" name="closedDate" placeholder="Closed Date & Time" value="{{ old('closedDate', $closedDate) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Opened Time</label>
                        <div class="col-sm-12 col-md-4">
                            <input class="form-control time-picker" name="openedTime" placeholder="Opened Date & Time" value="{{ old('openedTime', $openedTime) }}">
                        </div>
                        <label class="col-sm-12 col-md-2 col-form-label">Closed Time</label>
                        <div class="col-sm-12 col-md-4 pull-right">
                            <input class="form-control time-picker" name="closedTime" placeholder="Closed Date & Time" value="{{ old('closedTime', $closedTime) }}">
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-sm">Save All*</button>
                    </div>
                    <p class="text-danger font-weight-bold font-italic">*Edit purpose only</p>
                </form>
            </div>
            <div class="clearfix mb-0">
                <div class="pull-left mb-4">
                    <h4 class="text-blue h4">Display All Questions</h4>
                    <p>This page displays the information of all questions inside the package, including Questions, Answers, Quiz Duration, Score For Each Questions. In here, you can also create new question and edit your question</p>
                </div>
                <div class="pull-right">
                    <a href="{{ route('guru.soal.create', ['paket' => $paket->id]) }}" class="btn btn-primary btn-sm scroll-click">Create New</a>
                </div>
                <div class="pull-left mt-3">
                    <h6 class="text-blue">Total Questions: {{ count($soal) }}</h6>
                </div>
            </div>
        </div>
        <div class="pb-20">
            <table class="data-table table stripe hover nowrap">
                <thead>
                    <tr>
                        
                        <th scope="col">#</th>
                        <th scope="col">Question</th>
                        <th scope="col">Answer</th>
                        <th scope="col">Score</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($soal as $key => $item)
                        <tr>
                            <td class="table-plus" scope="row">{{ $key+1 }}</td>
                            <td>{{ $item->soal }}</td>
                            <td>{{ $item->jawaban }}</td>
                            <td>{{ $item->nilai }}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="dw dw-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                        <a class="dropdown-item" href="/guru/cek-paket/{{ $paket->id }}/{{ $item->id }}"><i class="dw dw-eye"></i> View Details</a>
                                        <a class="dropdown-item" href="/guru/cek-paket/{{ $paket->id }}/{{ $item->id }}"><i class="dw dw-edit2"></i> Edit Question</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No Questions</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
<!-- Responsive tables End -->
@endsection

@push('scripts')
<script src="{{ asset('deskapp/src/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/vendors/scripts/datatable-setting.js') }}"></script>
@endpush