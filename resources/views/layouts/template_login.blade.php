<!doctype html>
<html lang="en">


<!-- Mirrored from arasari.studio/wp-content/projects/forny/templates/03_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Feb 2021 12:13:55 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Authentication forms">
    <meta name="author" content="Arasari Studio">

    <title>Sign Yourself</title>
    <link rel="icon" href="{{ asset('deskapp/vendors/images/light-logo.png') }}" type="image/x-icon">
    <link href="{{ asset('template_login/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template_login/css/common.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/icon-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/vendors/styles/style.css') }}">


<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&amp;display=swap" rel="stylesheet">
<link href="{{ asset('template_login/css/theme-03.css') }}" rel="stylesheet">

</head>

<body>
    @yield('content')

    <script src="{{ asset('template_login/js/jquery.min.js') }}"></script>
    <script src="{{ asset('template_login/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template_login/js/main.js') }}"></script>
    <script src="{{ asset('template_login/js/demo.js') }}"></script>

</body>


<!-- Mirrored from arasari.studio/wp-content/projects/forny/templates/03_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Feb 2021 12:14:09 GMT -->
</html>
