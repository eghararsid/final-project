<div class="left-side-bar">
    <div class="brand-logo">
        <a href="/guru">
            <img src="{{ asset('deskapp/vendors/images/dark-logo.png') }}" alt="" class="dark-logo">
            <img src="{{ asset('deskapp/vendors/images/light-logo.png') }}" alt="" class="light-logo">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li class="dropdown">
                    <a href="/guru" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-house-1"></span><span class="mtext">Dashboard</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="{{ route('guru.paket.create') }}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-library"></span><span class="mtext">Create Package</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="{{ route('guru.paket.index') }}" class="dropdown-toggle no-arrow">
                        <span class="micon icon-copy ti-harddrives"></span><span class="mtext">All Packages</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="dropdown-toggle no-arrow">
                        <span class="micon icon-copy ti-harddrive"></span><span class="mtext">Incoming Packages</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="dropdown-toggle no-arrow">
                        <span class="micon icon-copy ti-harddrive"></span><span class="mtext">Passed Packages</span>
                    </a>
                </li>
                <li>
                    <div class="dropdown-divider"></div>
                </li>
                <li>
					<div class="sidebar-small-cap">Settings</div>
				</li>
                <li>
                    <a href="javascript:;" class="dropdown-toggle no-arrow">
                        <span class="micon icon-copy ion-android-person"></span><span class="mtext">Profile</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle no-arrow" href="javascript:;" data-toggle="right-sidebar">
                        <span class="micon dw dw-settings2"></span><span class="mtext">Preferences</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}" class="dropdown-toggle no-arrow" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <span class="micon dw dw-logout"></span><span class="mtext">Log Out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>