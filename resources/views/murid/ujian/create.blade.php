@extends('layouts.murid')

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mt-3">
        <form action="{{ route('murid.ujian.store') }}" method="POST">
            @csrf
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue h3">Join Quiz Forms</h4>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-12 col-md-2 col-form-label">Package Name</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="paket" placeholder="Input Package Name">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Package Password</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="password" placeholder="Input Package Password">
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Join Exam</button>
                </div>
            </div>
        </form>
    </div>
@endsection