@extends('layouts.murid')

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mt-3">
        <div class="clearfix">
            <div class="center">
                <p class="text-blue">Exam Finished At: <span class="text-blue h3">{{ $ujian->finished_at }}</span></p>
            </div>
        </div>
    </div>
    <div class="pd-20 card-box mt-3">
        <form action="{{ route('murid.ujian.update', ['paket' => $paket, 'ujian' => $ujian]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="clearfix">
                <div class="pull-left">
                    <h3 class="text-blue h3">Good Luck!</h3>
                </div>
            </div>
            @forelse ($paket->soal as $key => $item)
                <div class="mt-5">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h4 class="h4">Question {{ $key + 1 }}</h4>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-12">
                            <label class="form-control col-form-label">{{ $item->soal }}</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 col-md-12">
                            <input class="form-control" name="jawaban[]" placeholder="Enter Answer">
                        </div>
                    </div>
                </div>    
            @empty
                <h4 align="center">No Questions</h4>
            @endforelse
            <div class="clearfix">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
