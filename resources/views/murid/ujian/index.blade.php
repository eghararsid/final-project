@extends('layouts.murid')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css') }}">
@endpush

@section('content')
<!-- Responsive tables Start -->
<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h4 class="text-blue h4">Display All Examinations</h4>
            <p>This page displays the information of all Quiz you have done, including Quiz Name, Questions number and Students Joined the Quiz. In here, you can also join new quiz, view more details into your Quiz Recap and view ranking</p>
        </div>
        <div class="pull-right">
            <a href="{{ route('murid.ujian.create') }}" class="btn btn-primary btn-sm scroll-click">Join Quiz</a>
        </div>
    </div>
    <div class="pb-20">
        <table class="data-table table stripe hover nowrap">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Exams Name</th>
                    <th scope="col">Questions</th>
                    <th scope="col">Score</th>
                    <th scope="col">Started At</th>
                    <th scope="col">Closed At</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($ujian as $key => $item)
                    <tr>
                        <td class="table-plus" scope="row">{{ $key + 1 }}</td>
                        <td>{{ $paket[$key]->paket }}</td>
                        <td>{{ $soal[$key] }}</td>
                        <td>{{ $item->nilai }}</td>
                        <td>{{ $paket[$key]->mulai }}</td>
                        <td>{{ $paket[$key]->selesai }}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a href="{{ route('murid.ujian.show', ['paket' => $item->paket_id, 'ujian' => $item->id]) }}" class="dropdown-item"><i class="dw dw-edit2"></i> Join Exam</a>
                                    <a href="{{ route('murid.ranking.show', ['ranking' => $item->paket_id]) }}" class="dropdown-item"><i class="dw dw-eye"></i> View Ranking</a>
                                    <a href="{{ route('murid.penilaian.show', ['penilaian' => $item->paket_id]) }}" class="dropdown-item"><i class="dw dw-eye"></i> Inspect Result</a>
                                </div>
                            </div>
                        </td>
                    </tr>    
                @empty
                    <tr>
                        <td colspan="6" align="center">No Examinations Available</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
<!-- Responsive tables End -->
@endsection

@push('scripts')
<script src="{{ asset('deskapp/src/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('deskapp/vendors/scripts/datatable-setting.js') }}"></script>
@endpush