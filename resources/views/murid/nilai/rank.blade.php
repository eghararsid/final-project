@extends('layouts.murid')

@section('content')
<!-- Responsive tables Start -->
<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h4 class="text-blue h4">Display All Quiz</h4>
            <p>This page displays the information of all Quiz you have done, including Quiz Name, Questions number and Students Joined the Quiz. In here, you can also join new quiz, view more details into your Quiz Recap and view ranking</p>
        </div>
        <div class="pull-right">
            <a href="{{ route('murid.ujian.create') }}" class="btn btn-primary btn-sm scroll-click">Join Quiz</a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Score</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rank as $key => $item)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $name[$key] }}</td>
                        <td>{{ $item->nilai }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- Responsive tables End -->
@endsection