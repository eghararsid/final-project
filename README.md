Kelompok Nomor 1:
 - Akmal Rivai
 - Mohammed Farrell Eghar Syahputra
 - Muhamad Iqbal Fadilah

Tema Projek: Website Untuk Guru Dan Murid Melakukan Ujian Online Berbasis Essay

Template References:
 - Dashboard : https://themewagon.com/themes/free-responsive-bootstrap-4-html5-admin-dashboard-template-deskapp/'
 - Login : https://arasari.studio/wp-content/projects/forny/templates/03_login.html

Link Demo: https://drive.google.com/drive/folders/1da5U8jsyg4_wxgafWF50C8BlZDXXw-Ht?usp=sharing

Link Deploy: http://final-project.fadilah.my.id
